import PropTypes from 'prop-types';
import React from 'react';
import axios from 'axios';

import Layout from "../layout/Layout";
import {getCSRF} from "../utils/globals";

export default class Dashboard extends React.Component {
  static propTypes = {
    current_user: PropTypes.object.isRequired
  };

  state = {
    title: '',
    body: '',
    allPosts: [],
    myPosts: []
  };

  componentDidMount() {
    this.getPosts();
  }

  render() {
    const allPosts = this.state.allPosts.map((post, index) => {
      return(
        <tr key={post.id}>
          <th scope="row">{++index}</th>
          <td>{post.title}</td>
          <td>{post.body}</td>
          <td>{post.user_id}</td>
        </tr>
      )
    });

    const myPosts = this.state.myPosts.map((post, index) => {
      return(
        <tr key={post.id}>
          <th scope="row">{++index}</th>
          <td>{post.title}</td>
          <td>{post.body}</td>
          <td><button onClick={() => this.deletePost(post.id)} className="btn btn-danger btn-sm">Delete</button></td>
        </tr>
      )
    });

    return (
      <Layout>
        <h1>Dashboard</h1>
        <div className="row">
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-12">
                <form className="form-inline">
                  <div className="form-group mb-2">
                    <input name="title" onChange={this.handleInputChange} value={this.state.title} type="text"
                           className="form-control" placeholder="Title"/>
                  </div>
                  <div className="form-group mx-sm-3 mb-2">
                    <input name="body" onChange={this.handleInputChange} value={this.state.body} type="text"
                           className="form-control" placeholder="Body"/>
                  </div>
                  <button onClick={this.addPost} type="submit" className="btn btn-primary mb-2">Add Post</button>
                </form>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <table className="table table-sm table-striped">
                  <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th>
                    <th scope="col">Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  { myPosts }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <table className="table table-sm table-bordered">
              <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">User ID</th>
              </tr>
              </thead>
              <tbody>
              { allPosts }
              </tbody>
            </table>
          </div>
        </div>
      </Layout>
    )
  }

  deletePost = postId => {
    axios.post('/posts/update/' + postId, {}, getCSRF())
      .then(r => {
        const myPosts = this.state.myPosts.filter(post => post.id !== postId);
        const allPosts = this.state.myPosts.filter(post => post.id !== postId);

        this.setState({allPosts, myPosts});
      });
  };

  addPost = (e) => {
    e.preventDefault();
    if(this.state.title !== '' && this.state.body !== ''){
      const data = {post: {title: this.state.title, body: this.state.body}};
      axios.put('/posts/create', data, getCSRF())
        .then(r => {
          console.log(r.data);
          const myPosts = [...this.state.myPosts];
          const allPosts = [...this.state.allPosts];
          myPosts.push(r.data);
          allPosts.push(r.data);

          this.setState({allPosts, myPosts});
        });
    }
  };

  getPosts = () => {
    axios.get('/posts/index').then(r => {
      console.log(r);
      const myPosts = r.data.filter(post => post.user_id === this.props.current_user.id);

      this.setState({allPosts: r.data, myPosts});
    });
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
}