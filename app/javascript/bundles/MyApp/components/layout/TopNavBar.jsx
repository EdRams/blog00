import React from 'react';

const TopNavBar = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light rounded">
        <a className="navbar-brand" href="#">Home</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09"
                aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"/>
        </button>

        <div className="collapse navbar-collapse" id="navbarsExample09">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
            </li>
          </ul>
          <ul className="navbar-nav align-content-end">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">Dropdown</a>
              <div className="dropdown-menu" aria-labelledby="dropdown09">
                <a className="dropdown-item" rel="nofollow" data-method="delete" href="/users/sign_out">Logout</a>
              </div>
            </li>
          </ul>

        </div>
      </nav>
    </div>
  )
}

export default TopNavBar;