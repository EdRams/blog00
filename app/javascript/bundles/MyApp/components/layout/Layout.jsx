import PropTypes from 'prop-types';
import React from 'react';

import TopNavBar from "./TopNavBar";

export default class Layout extends React.Component {
  render() {
    return (
      <React.Fragment>
        <TopNavBar/>

        {this.props.children}
      </React.Fragment>
    )
  }
}