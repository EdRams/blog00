import ReactOnRails from 'react-on-rails';

import Dashboard from "../bundles/MyApp/components/pages/Dashboard";

ReactOnRails.register({
  Dashboard,
});