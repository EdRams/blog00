class PostsController < ApplicationController

  # PUT '/posts/create'
  def create
    post = Post.new(post_params)
    post.user_id = current_user.id
    post.state = 'Enabled'

    if post.save!
      render json: post
    end
  end

  # POST '/posts/update/:post_id'
  def update
    post_id = params[:post_id]
    post = Post.find(post_id)
    post.update(state: 'Disabled')
  end

  def index
    render json: prepare_posts(Post.where(state: 'Enabled'))
  end


  private

  def prepare_posts(posts)
    posts_view = []

    posts.each do |post|
      obj = {
        id: post.id,
        user_id: post.user_id,
        title: post.title,
        body: post.body,
        image: post.image,
        state: post.state
      }
      posts_view.push(obj)
    end

    posts_view
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end
end