class UsersController < ApplicationController
  def dashboard
    user = User.find(current_user.id)
    @current_user = { id: user.id }
  end
end
