Rails.application.routes.draw do
  post '/posts/update/:post_id', to: 'posts#update'
  put '/posts/create'
  get '/posts/index'
  get 'users/dashboard'

  devise_for :users

  root to: 'pages#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
