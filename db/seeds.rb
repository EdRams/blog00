users = [
  {email: "javaimagination@gmail.com", encrypted_password: "$2a$12$lxY2ihzyxaeCN71cs5.sFezRQP.Kw30/qLkEBEnajuwD/iC5G1Ei.", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil},
  {email: "eduardo@pixiebob.io", encrypted_password: "$2a$12$r9nffHc6scgNyr7pJMnXP.1j9r9ngiFldV4BSn02DjTn7fge3lQsm", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil}
]

users.each do |u|
  user = User.new
  user.email = u[:email]
  user.encrypted_password = u[:encrypted_password]
  user.save!(validate: false)
end


Post.create!([
  {title: "Megaman", body: "Ago With Megaman", image: nil, state: "Enabled", user_id: 1},
  {title: "Good Monday", body: "This post is about to have a great Monday, with sun like this or not :)", image: nil, state: "Disabled", user_id: 1},
  {title: "Eduardo Pixiebob", body: "Esta es una gran historia en Español :)", image: nil, state: "Enabled", user_id: 2},
  {title: "Activity Builder", body: "Algun dia deberian estudiar el Activity Buidler, sobre todo porque tiene unas cosas buenas.", image: nil, state: "Enabled", user_id: 2},
  {title: "iPhone", body: "Girl you know the reason Why", image: nil, state: "Enabled", user_id: 2},
  {title: "Rain", body: "It's a great by Beatles", image: nil, state: "Enabled", user_id: 1},
  {title: "Robot Post", body: "Este es un post automatico creado para Cypress", image: nil, state: "Enabled", user_id: 1},
  {title: "The Beatles", body: "Es un dia en el que estamos probando Cypress 5.0.0", image: nil, state: "Enabled", user_id: 1}
])
