describe('e2e Add Post', () => {
  it('should add a post', () => {
    cy.visit('/users/sign_in');

    cy.get('#user_email').type('javaimagination@gmail.com')

    // {enter} causes the form to submit
    cy.get('#user_password').type(`${'123456'}{enter}`)

    // we should be redirected to /dashboard
    cy.url().should('include', 'users/dashboard')

    // our auth cookie should be present
    cy.getCookie('_blog00_session').should('exist')

    // UI should reflect this user being logged in
    cy.get('h1').should('contain', 'Dashboard')

    /*cy.get('#user_email').click();
    cy.get('#user_email').type('javaimagination@gmail.com');
    cy.get('#user_password').type('123456');
    cy.get('.actions > input').click();
    cy.get('#new_user').submit();

    cy.url().should('contains', 'http://localhost:3000/users/dashboard');
    cy.get('.form-group:nth-child(1) > .form-control').click();
    cy.get('.form-group:nth-child(1) > .form-control').type('The Beatles');
    cy.get('.mx-sm-3 > .form-control').type('{backspace}');
    cy.get('.mx-sm-3 > .form-control').type('Es un dia en el que estamos probando Cypress 5.0.0');
    cy.get('.btn-primary').click();*/

  });
})